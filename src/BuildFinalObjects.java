import java.util.ArrayList;
import java.util.List;


public class BuildFinalObjects {

	public static void main(String[] args) {
		System.out.println("Hello !");
		
		List<FinalInterface> l = new ArrayList<FinalInterface>();
		
		for (int i = 0; i < 10; i++) {
			l.add(buildInterface(i));
		}
		
		for (FinalInterface finalInterface : l) {
			System.out.println(finalInterface.getInt());
		}
		
	}

	private static FinalInterface buildInterface(final int i) {
		return new FinalInterface() {
			
			@Override
			public int getInt() {
				return i;
			}
		};
	}

}
